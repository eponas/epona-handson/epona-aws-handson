# AWS CLI を設定するための環境変数

## 環境変数の設定方法

デフォルトのユーザーの環境変数を設定する方法を示します。

### Linux or macOS

```shell
$ export AWS_ACCESS_KEY_ID=[AWSアクセスキー]
$ export AWS_SECRET_ACCESS_KEY=[AWSシークレットアクセスキー]
$ export AWS_DEFAULT_REGION=[リージョン]
```

環境変数を設定するとシェルセッションの終了時、または変数に別の値を設定するまで使用する値が変更されます。

### Windows Command Prompt

```bash
C:\> set AWS_ACCESS_KEY_ID=[AWSアクセスキー]
C:\> set AWS_SECRET_ACCESS_KEY=[AWSシークレットアクセスキー]
C:\> set AWS_DEFAULT_REGION=[リージョン]
```

`set`を使用して環境変数を設定すると現在のコマンドプロンプトセッションの終了時、または変数を別の値に設定するまで使用する値が変更されます。

### PowerShell

```bash
PS C:\> $Env:AWS_ACCESS_KEY_ID="[AWSアクセスキー]"
PS C:\> $Env:AWS_SECRET_ACCESS_KEY="[AWSシークレットアクセスキー]"
PS C:\> $Env:AWS_DEFAULT_REGION="[リージョン]"
```

PowerShellプロンプトで環境変数を設定した場合は、現在のセッションの期間だけ値が保存されます。
